package ru.maa.calculator.calc;

import java.util.Scanner;

public class calc {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int num1, num2;

        System.out.printf("Выберите действие с числами:%n1. Сложение%n2. Вычетание%n3. Умножение%n4. Возведение в степень%n5. Деление%n6. Выделение остатка%nНомер вашего действия: ");
        int pick = scanner.nextInt();
        System.out.print("Введите числа: ");
        num1 = scanner.nextInt();
        num2 = scanner.nextInt();

        if (pick != 0 || pick < 8) {
            if (pick == 1) {
                int result = MathInt.add(num1, num2);
                System.out.printf("Сумма=" + result);
            }
            if (pick == 2) {
                int result = MathInt.sub(num1, num2);
                System.out.printf("Разность=" + result);
            }
            if (pick == 3) {
                int result = MathInt.multiplication(num1, num2);
                System.out.print("Произведение=" + result);
            }
            if (pick == 4) {
                  if (num2<0){
                    System.out.println("Неверная степень");
                    return;
                  }
                int result = MathInt.pow(num1, num2);
                System.out.print(num1 + " в степени " + num2 + "=" + result);
            }
            if (pick == 5) {
                double result = MathInt.div(num1, num2);
                System.out.print("Деление=" + result);
            }

            if (pick == 6) {
                int result = MathInt.divEnd(num1, num2);
                System.out.print("Остаток=" + result);
            }
        } else {
            System.out.println("Неверный выбор действия");
        }

    }
}

