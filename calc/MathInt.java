package ru.maa.calculator.calc;

public class MathInt {
    /**
     * Находление суммы
     *
     * @param a Первое слагаемое
     * @param b Второе слагаемое
     * @return Сумму
     */
    static int add(int a, int b){
        return a + b;
    }

    /**
     * Нахождение разности
     *
     * @param a Уменьшаемое
     * @param b Вычитаемое
     * @return Разность
     */

    static int sub(int a, int b){
        return a - b;
    }

    /**
     * Нахождение произведения
     *
     * @param a Первый множитель
     * @param b Второй множитель
     * @return Произведение
     */

    static int multiplication(int a, int b){
        return a * b;
    }

    /**
     * Возведение в степень
     *
     * @param a Основание степени
     * @param b Показатель степени
     * @return Число возведенное в степень
     */

    static int pow(int a, int b){
        int f = 1;

        for (int i = 0; i < b; i++) {
            f = f * a;
        }

        if (b<0){
            f=1/f;
        }
        return f;
    }

    /**
     * Деление чисел
     *
     * @param a Делимое число
     * @param b Делитель
     * @return Частное
     */

    static int div(int a, int b){
        return a / b;
    }

    /**
     * Нахождение остатка
     *
     * @param a Делимое число
     * @param b Делитель
     * @return Остаток
     */
    static int divEnd(int a, int b){
        return a % b;
    }
}

